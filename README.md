Clear Minds Talking Therapies is a counselling service for individuals and couples/families. We create opportunities for relationship and life success. We empower people to create happy, loving, and fulfilling relationships and lives. Our counselling style is Cognitive Behavioural Therapy (CBT).

Address: 12 Vincent St, Saint Helens WA10 1LF, United Kingdom

Phone: +44 1744 636574

Website: [https://clearmindsltd.co.uk](https://clearmindsltd.co.uk)
